# Conversation Dataset

This repository contains .csv files of conversations that were produced by SyniaAI, an experimental closed-source machine learning project designed for end-to-end conversations.

During the time that this project is training large amounts of data, i have created a small program that will take conversation logs of the bot talking to itself and export it as a CSV file. This program will automatically commit the updates to this repository.

## Format

The format for these datasets are simple, the first column is the input and the second column is the output. on each file the first row will always start with an initial question such as "*Hello*", and the proceeding rows are the continuation of the conversation.

![An example of how the conversations are created](https://i.postimg.cc/dQnPkTTN/example.png)

## Support languages

| Language | Directory Name | Status       |
|----------|----------------|--------------|
| English  | /src/en        | Available    |
| Spanish  | N/A            | Coming soon  |

## Author

This project and SyniaAI was created by Narrakas (NETKAS)
QQ: [203818872](mailto:203818872@qq.com)

## License
You can use this for whatever you want for as long as you aren't selling this data, you don't need to provide credit. However, it wouldn't be hurtful  to provide credit. :-)
